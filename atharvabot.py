import requests, json
def response(text, id, base):
    if text:
        reply = text
    else:
        reply = "Hello!"
    url = base + f"sendMessage?chat_id={id}&text={reply}"
    requests.get(url)
sessions = {}

token = "1117178202:AAFQ-eoR5OTdwlTfpD31G6z_Q80feJAK7GQ"

base = f"https://api.telegram.org/bot{token}/"
offset = None
print("Use 'CTRL + C' to end the script!")
while True:
    if offset:
        url = base + f"getUpdates?offset={offset + 1}"
    else:
        url = base + "getUpdates"
    updates = json.loads(requests.get(url).content)["result"]
    for item in updates:
        offset = item["update_id"]
        try:
            text = item["message"]["text"]
        except:
            text = None
        id = item["message"]["chat"]["id"]
        if text == "hi" or text == "Hi" or text == "hI" or text == "HI":
            if id in sessions:
                if sessions[id][1] == 1:
                    text = f" {sessions[id][0]} you had already said hi once before!"
                else:
                    text = f"{sessions[id][0]} you had already said hi {sessions[id][1]} times!"
                    
                sessions[id] = [item["message"]["chat"]["first_name"], sessions[id][1] + 1]
            else:
                sessions[id] = [item["message"]["chat"]["first_name"], 1]
        print(sessions,"     ",text)
        response(text, id, base)

